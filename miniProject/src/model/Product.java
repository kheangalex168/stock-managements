package model;

public class Product {
    private int ID;
    private String name;
    private double unitPrice;
    private int qty;
    private String importDate;

    public Product() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getImportDate() {
        return importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    @Override
    public String toString() {
        return this.ID + "," +
                this.name + "," +
                this.unitPrice + "," +
                this.qty + "," +
                this.importDate;
    }

    public Product(int ID, String name, double unitPrice, int qty, String importDate) {
        this.ID = ID;
        this.name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.importDate = importDate;
    }
}
