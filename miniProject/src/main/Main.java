package main;

import db.DbOperation;
import helper.H;
import model.Product;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    static int perPage = 5;
    static int page = 1;

    public static void main(String[] args) throws IOException {
        Header();
        File dbFile = new File(Const.dbPath);
        while (true) {
            String menuChoice = H.getChoice();
            switch (menuChoice) {
                case "*" -> displayProduct(dbFile, page);
                case "W", "w" -> writeProduct();
                case "R", "r" -> readProduct();
                case "U", "u" -> updateProduct();
                case "D", "d" -> deleteProduct();
                case "F", "f" -> moveToFirst();
                case "P", "p" -> moveToPrevious();
                case "N", "n" -> moveToNext();
                case "L", "l" -> moveToLast();
                case "G", "g" -> gotoPage();
                case "S", "s" -> searchWildcard();
                case "SE", "se", "Se", "sE" -> setRow();
                case "SA", "sa", "Sa", "sA" -> System.out.println("This program is Auto-save to File");
                case "RE", "re", "Re", "rE" -> restore();
                case "BA", "ba", "Ba", "bA" -> backup();
                case "_10M" -> _10M();
                case "E", "e" -> exit();
            }
            printMainMenu();
        }
    }

    private static void backup() throws IOException {
        Path source = Paths.get(Const.dbPath);
        Path destination = Paths.get("C:/java_db_backup/%s".formatted(H.getCurrentTimeAndDate()));
        Files.copy(source, destination , StandardCopyOption.REPLACE_EXISTING);
        H.printWaveStyle(94);
        System.out.printf("\tBackup Successfully in File Backup %s.bak%n", H.getCurrentTimeAndDate());
        H.printWaveStyle(94);
    }

    private static void restore() throws IOException {
        System.out.println("******** Please choose file Backup ********");
        List<Path> fileList = Files.walk(Paths.get("C:/java_db_backup/"))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());
        for (int i = 0; i < fileList.size(); i++) {
            System.out.printf("%d) %s%n", i + 1, fileList.get(i).getFileName());
        }
        int fileChoice = H.getInt("Choose file Now: ",1, fileList.size());
        Path path = Paths.get(Const.dbPath);
        Files.copy(fileList.get(fileChoice-1).toAbsolutePath(), path, StandardCopyOption.REPLACE_EXISTING);
        H.printWaveStyle(25);
        System.out.println("\tRestore successfully");
        H.printWaveStyle(25);
    }

    private static void _10M() throws IOException {
        long startTime = System.currentTimeMillis();
        DbOperation.initializeFile(10e6);
        System.out.println("Total WRITE Time: "+(System.currentTimeMillis() - startTime));

        startTime = System.currentTimeMillis();
        FileReader fileReader = new FileReader(Const.dbPath);
        BufferedReader bufferedWriter = new BufferedReader(fileReader);

        while (bufferedWriter.readLine() != null){ }
        System.out.println("Total Read Time: "+(System.currentTimeMillis() - startTime));
    }

    private static void exit() {
        System.out.println("Good Bye !!!");
        System.exit(0);
    }

    private static void setRow() {
        perPage = H.getInt("Please enter row for Display: ", 1, Integer.MAX_VALUE);
        DbOperation.readAllData(
                        Const.column,
                new File(Const.dbPath),
                perPage,
                Main.page
        );
        H.printWaveStyle(25);
        System.out.printf("\tSet row to %d Successfully%n", Main.perPage);
        H.printWaveStyle(25);
    }

    private static void searchWildcard() throws FileNotFoundException {
        String productName = H.getString("Search By Name: ");
        File file = new File(Const.dbPath);
        Scanner db = new Scanner(file);
        String separator = Const.separator;
        while (db.hasNextLine()) {
            String thisLine = db.nextLine();
            if (thisLine.split(separator)[1].contains(productName)) {
                DbOperation.productStrPreview(separator, thisLine);
            }
        }
    }

    private static void gotoPage() {
        Main.page = H.getInt("Go to Page: ", 1, Integer.MAX_VALUE);
        DbOperation.readAllData(Const.column, new File(Const.dbPath), perPage, Main.page);
    }

    private static void moveToNext() throws IOException {
        int row = DbOperation.countLine(Const.dbPath) - 1;
        ++page;
        int currentPage = row % perPage == 0 ? row / perPage : (row / perPage) + 1;

        if (currentPage < page) {
            DbOperation.readAllData(
                    Const.column,
                    new File(Const.dbPath),
                    perPage,
                    --page
            );
        } else {
            DbOperation.readAllData(
                    Const.column,
                    new File(Const.dbPath),
                    perPage,
                    page
            );
        }
    }

    private static void moveToLast() throws IOException {
//        allRow % perPage
        int row = DbOperation.countLine(Const.dbPath) - 1;
        Main.page = row % perPage == 0 ? row / perPage : (row / perPage) + 1;
        DbOperation.readAllData(
                Const.column,
                new File(Const.dbPath),
                perPage,
                Main.page
        );
    }

    private static void moveToPrevious() {
        Main.page = (Main.page != 1) ? --Main.page : 1;
        DbOperation.readAllData(
                Const.column,
                new File(Const.dbPath),
                perPage,
                page
        );
    }

    private static void readProduct() throws FileNotFoundException {
        DbOperation.printProductByID("Read by ID : ");
    }

    private static void writeProduct() throws IOException {
        DbOperation.addRecord(H.productPreview(tempNewProduct()));
    }

    private static void displayProduct(File dbFile, int i) {
        DbOperation.readAllData(Const.column, dbFile, perPage, i);
    }

    private static void moveToFirst() {
        displayProduct(new File(Const.dbPath), 1);
    }

    private static void deleteProduct() throws FileNotFoundException {
        Product product1 = DbOperation.getProductByID("Please input ID of Product : ");
        H.productPreview(product1);
        String str = H.getString("Are you sure want to add this record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
            String strID1 = String.valueOf(product1.getID());
            String data = readDataFromFile().toString();
            String row = DbOperation.getLineByID(strID1) + "\n";
            try {
                FileWriter fileWriter1 = new FileWriter(Const.dbPath);
                fileWriter1.write(data.replace(row, ""));
                fileWriter1.close();
                H.printWaveStyle(25);
                System.out.println("\tProduct was removed.");
                H.printWaveStyle(25);
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }

    private static void updateProduct() throws IOException {
        Product product = DbOperation.getProductByID("Please input ID of Product : ");
        H.productPreview(product);
        System.out.println("What do you want to update");
        System.out.println("╔═════════════════════════════════════════════════════════════════════╗");
        System.out.println("║  1.All   | 2.Name  | 3.Quantity | 4.Unit Price  5. Back to menu     ║");
        System.out.println("╚═════════════════════════════════════════════════════════════════════╝");
        int updateChoice = H.getInt("Option (1-5) : ", 1, 5);
        String strID = String.valueOf(product.getID());
        switch (updateChoice) {
            case 1:
                DbOperation.updateFullRecord(product, DbOperation.getLineByID(strID));
                break;
            case 2:
                DbOperation.updateRecordName(product, DbOperation.getLineByID(strID));
                break;
            case 3:
                DbOperation.updateRecordQty(product, DbOperation.getLineByID(strID));
                break;
            case 4:
                DbOperation.updateRecordUnitPrice(product, DbOperation.getLineByID(strID));
                break;
            default:
                break;
        }
    }

    private static void printMainMenu() {
        System.out.println("╔══════════════════════════════════════════════════════════════════════════════════════════════╗");
        System.out.println("║  *)Display | W)write | R)ead    | U)pdate | D)elete | F)irst    | P)revious | N)ext | L)ast  ║");
        System.out.println("║  S)earch   | G)o to  | Se)t row | Sa)ve   | B)ackup | Re)store  | H)elp     | E)xit          ║");
        System.out.println("╚══════════════════════════════════════════════════════════════════════════════════════════════╝");
    }

    private static StringBuilder readDataFromFile() {
        StringBuilder data = new StringBuilder();
        try {
            File file = new File(Const.dbPath);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                data.append(scanner.nextLine()).append('\n');
            }
            scanner.close();
            return data;
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return data;
    }


    private static Product tempNewProduct() throws IOException {
        Product product = new Product();
        product.setID(Integer.parseInt(DbOperation.getLastID()) + 1);
        product.setName(H.getString("Product's name : "));
        product.setUnitPrice(H.getDouble("Product's Price : ", 1, Double.MAX_VALUE));
        product.setQty(H.getInt("Product's Qty : ", 0, Integer.MAX_VALUE));
        product.setImportDate(H.getCurrentDate());
        return product;
    }

    private static void Header() {
        H.printWithTab(12, "Welcome to", true);
        H.printWithTab(11, "Stock Management\n", true);
        H.asciiGenerator(100, 30, "SiemReabG1", 100);
        String text = "\nPlease wait Loading...";
        H.printThread(text + "\n", 100);
        printMainMenu();
    }
}
