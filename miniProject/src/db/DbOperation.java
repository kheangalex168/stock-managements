package db;

import helper.H;
import main.Const;
import model.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

import static helper.H.getString;
import static helper.H.printWaveStyle;

public class DbOperation {



    public static void updateRecordUnitPrice(Product product, String oldRow) throws IOException {
        double productQty = H.getDouble("Product's Price : ",1, Integer.MAX_VALUE);
        product.setUnitPrice(productQty);
        String str = getString("Are you sure want to add update record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")){
            addProductToFile(product, oldRow);
        }
    }

    public static void updateRecordQty(Product product, String oldRow) throws IOException {
        int productQty = H.getInt("Stock Quantity : ",1, Integer.MAX_VALUE);
        product.setQty(productQty);
        String str = getString("Are you sure want to add update record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")){
            addProductToFile(product, oldRow);
        }
    }

    public static void updateRecordName(Product product, String oldRow) throws IOException {
        String productName = H.getString("Product's Name : ");
        product.setName(productName);
        String str = getString("Are you sure want to add update record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")){
            addProductToFile(product, oldRow);
        }
    }

    private static void addProductToFile(Product product, String oldRow) throws IOException {
        H.productPreview(product);
        BufferedReader reader = new BufferedReader(new FileReader(Const.dbPath));
        String line;
        StringBuilder oldText = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            oldText.append(line).append("\n");
        }
        reader.close();
        String update = oldText.toString().replaceAll(oldRow, product.toString());
        FileWriter writer = new FileWriter(Const.dbPath);
        writer.write(update);
        writer.close();
        H.printWaveStyle(25);
        System.out.println("\tProduct was updated");
        H.printWaveStyle(25);
    }

    public static void updateFullRecord(Product product, String oldRow) throws IOException {
        String productName = H.getString("Product's Name : ");
        double productPrice = H.getDouble("Product's Price : ", 1, Double.MAX_VALUE);
        int productQty = H.getInt("Stock Quantity : ", 0, Integer.MAX_VALUE);
        product.setName(productName);
        product.setUnitPrice(productPrice);
        product.setQty(productQty);
        String str = getString("Are you sure want to update this record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")){
            addProductToFile(product, oldRow);
        }

    }

    public static String getLineByID(String text) throws FileNotFoundException {
        File file = new File(Const.dbPath);
        Scanner db = new Scanner(file);
        String separator = Const.separator;
        String strID = String.valueOf(text);
        while (db.hasNextLine()) {
            String thisLine = db.nextLine();
            if (thisLine.split(separator)[0].equalsIgnoreCase(strID)) {
                return thisLine;
            }
        }
        return null;
    }

    public static Product getProductByID(String text) throws FileNotFoundException {
        int id = H.getInt(text, 1, Integer.MAX_VALUE);
        File file = new File(Const.dbPath);
        Scanner db = new Scanner(file);
        String separator = Const.separator;
        String strID = String.valueOf(id);
        while (db.hasNextLine()) {
            String thisLine = db.nextLine();
            if (thisLine.split(separator)[0].equalsIgnoreCase(strID)) {
                String[] product = thisLine.split(separator);
                return new Product(
                        Integer.parseInt(product[0]),
                        product[1],
                        Double.parseDouble(product[2]),
                        Integer.parseInt(product[3]),
                        product[4]
                );
            }
        }
        return new Product();
    }

    public static void printProductByID(String text) throws FileNotFoundException {
        int id = H.getInt(text, 1, Integer.MAX_VALUE);
        File file = new File(Const.dbPath);
        Scanner db = new Scanner(file);
        String separator = Const.separator;
        String strID = String.valueOf(id);
        while (db.hasNextLine()) {
            String thisLine = db.nextLine();
            if (thisLine.split(separator)[0].equalsIgnoreCase(strID)) {
                productStrPreview (separator, thisLine);
            }
        }
    }

    public static void productStrPreview(String separator, String thisLine) {
        Table table = new Table(
                1,
                BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND
        );
        table.setColumnWidth(0, 30, 30);
        String[] thisRow = thisLine.split(separator);
        table.addCell("ID             : " + thisRow[0]);
        table.addCell("Name           : " + thisRow[1]);
        table.addCell("Unit Price     : " + thisRow[2]);
        table.addCell("Qty            : " + thisRow[3]);
        table.addCell("Import Date    : " + thisRow[4]);
        System.out.println(table.render());
    }

    public static void readAllData(String[] column, File file, int perPage, int page) {
        try {
            int row = 0;
            Scanner db = new Scanner(file);

            CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.center);
            Table t = new Table(
                    5,
                    BorderStyle.UNICODE_BOX_DOUBLE_BORDER);

            t.addCell(column[0], cellStyle);
            t.addCell(column[1], cellStyle);
            t.addCell(column[2], cellStyle);
            t.addCell(column[3], cellStyle);
            t.addCell(column[4], cellStyle);

            t.setColumnWidth(0, 18, 18);
            t.setColumnWidth(1, 18, 18);
            t.setColumnWidth(2, 18, 18);
            t.setColumnWidth(3, 18, 18);
            t.setColumnWidth(4, 18, 18);

            while (db.hasNext()) {
                String str = db.nextLine();
                if (row >= (perPage*(page-1))+1 && row <= page*perPage) {
                    String[] content = str.split(",");
                    t.addCell(content[0], cellStyle);
                    t.addCell(content[1], cellStyle);
                    t.addCell(content[2], cellStyle);
                    t.addCell(content[3], cellStyle);
                    t.addCell(content[4], cellStyle);
                }
                if (row > page*perPage) break;
                ++row;
            }
            System.out.println(t.render());
            int allRow = DbOperation.countLine(Const.dbPath)-1;
            printWaveStyle(94);
            int lastPage = allRow % perPage == 0 ? (allRow / perPage) : (allRow / perPage)+1;
            System.out.printf("Page %d of %d", page,lastPage);
            H.printWithTab(14, " ", false);
            System.out.printf("Total Record: %d%n", allRow);
            printWaveStyle(94);
            db.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addRecord(Product product) {
        String str = getString("Are you sure want to add this record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
            try {
                FileWriter fileWriter = new FileWriter(Const.dbPath, true);
                fileWriter.write(
                        "%d,%s,%s,%d,%s".formatted(
                                product.getID(),
                                product.getName(),
                                product.getUnitPrice(),
                                product.getQty(),
                                product.getImportDate())
                );
                fileWriter.close();
                printWaveStyle(28);
                System.out.printf("%d was add successfully%n", product.getID());
                printWaveStyle(28);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void initializeFile(double max) {
        Random random = new Random();
        try {
            FileWriter fileWriter = new FileWriter("c:/java_db_file/db.txt");
            fileWriter.write("id,name,unit-price,qty,import-date\n");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            String currentDate = H.getCurrentDate();
            for (int i = 0; i < max; i++) {
                int randQty = random.nextInt(3 - 1 + 1) + 1;
                bufferedWriter.write(i + 1 + "," + "product" + i + "," + 500 * randQty + "," + randQty + "," + currentDate + "\n");
            }
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int countLine(String filename) throws IOException {
        try (InputStream is = new BufferedInputStream(new FileInputStream(filename))) {
            byte[] c = new byte[1024];

            int readChars = is.read(c);
            if (readChars == -1) {
                // bail out if nothing to read
                return 0;
            }

            // make it easy for the optimizer to tune this loop
            int count = 0;
            while (readChars == 1024) {
                for (int i = 0; i < 1024; ) {
                    if (c[i++] == '\n') {
                        ++count;
                    }
                }
                readChars = is.read(c);
            }

            // count remaining characters
            while (readChars != -1) {
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
                readChars = is.read(c);
            }

            return count == 0 ? 1 : count;
        }
    }

    public static String getLastID() throws IOException {
        return getLastRow()[0];
    }

    public static String[] getLastRow() throws IOException {
        BufferedReader input = new BufferedReader(new FileReader(Const.dbPath));
        String last, line;
        last = "";
        while ((line = input.readLine()) != null) {
            last = line;
        }
        return last.split(Const.separator);
    }
}
